var jsonWebToken    = require('jsonwebtoken'),
    config          = require('./config'),

    WSS              = require('./server'),
    io               = new WSS(config),

    express         = require('express'),
    app             = express();


app
    .use(express.static(__dirname + '/client'))
    .get('/', function(req, res) {
        res.cookie(
            config.cookie,
            jsonWebToken.sign({user: 'info'}, config.secret),
            {httpOnly: true}
        );

        res.sendFile(__dirname + '/client/example.html');
    })
    .listen(1337, function() {
        console.log('example run on http://127.0.0.1:1337');
    });


var incomingName    = 'hello',
    outgoingName    = 'hi',
    outgoingMsg     = {foo: 'bar'};

io.on('connection', function(socket) {
    socket.on(incomingName, function(data) {
        console.log('incoming event => name: ' + incomingName + '; body:', data);

        socket.emit(outgoingName, outgoingMsg);
        console.log('outgoing event => name: ' + outgoingName + '; body:', outgoingMsg);
    });
});