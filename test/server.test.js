var assert          = require('chai').assert,
    config          = require('../config'),

    WSS             = require('../server'),
    io              = new WSS(config),

    EventEmitter    = require('events').EventEmitter,
    jsonWebToken    = require('jsonwebtoken'),
    WebSocketServer = require('ws').Server,

    ATTEMPTS        = 10;

describe('ws server', function() {
    it('should set default attempts to ATTEMPTS', function() {
        assert.equal(io._attempts, ATTEMPTS);
    });

    it('should throw when options invalid', function() {
        var badValues = [
                undefined,
                null,
                {},
                false,
                NaN
            ],

            badValuesForNumberField = badValues.concat(badValues, 'string'),
            badValuesForStringField = badValues.concat(badValues, 123),

            goodSecret  = 'secret',
            goodHost    = '127.0.0.1',
            goodCookie  = 'foo',
            goodPort    = 3001,

            stringFields = [
                'secret',
                'host',
                'cookie'
            ],

            numFields = [
                'port'
            ];

        test(stringFields, badValuesForStringField);
        test(numFields, badValuesForNumberField);

        function test(fields, values) {
            fields.forEach(function(field) {
                values.forEach(function(value) {
                    var badConfig = {
                        secret: goodSecret,
                        host:   goodHost,
                        cookie: goodCookie,
                        port:   goodPort
                    };

                    badConfig[field] = value;

                    try {
                        assert.throws(function() { new WSS(badConfig); });
                    } catch (err) {
                        console.log(err);
                        console.log('bad config:', badConfig);
                        assert.isUndefined(err);
                    }
                })
            });
        }
    });

    it('should have sockets array', function() {
        assert.isArray(io._sockets);
    });

    it('should can verify client by auth token', function() {
        var mockCookie = 'khal=drogo;' + config.cookie + '=' + jsonWebToken.sign({user: 'info'}, config.secret) + ';chuck=norris',
            mockClientInfo = {
            req: {
                headers: {
                    cookie: mockCookie
                }
            }
        };

        assert.isTrue(io._verifyClient(mockClientInfo));
    });

    it('should create socket object for connection', function(done) {
        var eventEmitter    = new EventEmitter(),
            callback        = io._handleConnection(function(socket) {
                assert.instanceOf(socket, EventEmitter);
                assert.lengthOf(io._sockets, 1);
                io._sockets = [];
                done();
            });

        assert.isFunction(callback);
        callback(eventEmitter);
    });

    it('should emit event to all sockets', function(done) {
        var eventEmitter    = new EventEmitter(),
            check           = 'test';

        eventEmitter.on('test', function(data) {
            assert.equal(data, check);
            io._sockets = [];
            done();
        });
        eventEmitter.send = function(data) {
            data = JSON.parse(data);
            eventEmitter.emit(data.name, data.data);
        };

        var callback = io._handleConnection(function() {});
        callback(eventEmitter);

        io.emit('test', check);
    });

    it('should subscribe on connection event', function(done) {
        var eventEmitter = new EventEmitter();
        eventEmitter.test = 'test';

        io.on('any', function() {
            //it is should not run
            assert.isTrue(false);
        });

        io.on('connection', function(socket) {
            assert.instanceOf(socket, EventEmitter);
            assert.equal(socket._ws.test, eventEmitter.test);
            io._sockets = [];
            done();
        });

        assert.lengthOf(io._wss.listeners('any'), 0);
        assert.lengthOf(io._wss.listeners('connection'), 1);

        io._wss.listeners('connection')[0](eventEmitter);
    });

    it('should have WebSocketServer instance', function() {
        assert.instanceOf(io._wss, WebSocketServer);
    });

    it('should have ws instances ' +
    'that should be event emitters, ' +
    'should inherit attempts amount, ' +
    'should have saved emit func from event emitter', function(done) {

        var eventEmitter = new EventEmitter();

        io._handleConnection(function(socket) {
            assert.instanceOf(socket, EventEmitter);
            assert.equal(socket._ws, eventEmitter);
            assert.equal(socket._attempts, ATTEMPTS);
            assert.equal(socket._emit, eventEmitter.emit);

            io._sockets = [];
            done();

        })(eventEmitter);
    });

    it('should have ws instances that should handle message', function(done) {
        var eventEmitter    = new EventEmitter(),
            msg             = JSON.stringify({name: 'foo', data: 'bar'});

        io._handleConnection(function(socket) {
            socket.on('foo', function(data) {
                assert.equal(data, 'bar');
                io._sockets = [];
                done();
            });

            socket._handleMessage(msg);
        })(eventEmitter);
    });

    it('should have ws instances that should parse message', function(done) {
        var eventEmitter    = new EventEmitter(),
            msg             = {name: 'foo', data: 'bar'},
            stringifiedMsg  = JSON.stringify(msg),

            badMsgs = [
                undefined,
                null,
                123,
                NaN,
                'string',
                function() {},
                [],
                {},
                {name: 'foo'},
                JSON.stringify({name: 'foo'}),
                {body: 'bar'},
                JSON.stringify({body: 'bar'})
            ];

        io._handleConnection(function(socket) {
            var m0 = socket._parseMessage(stringifiedMsg);
            assert.deepEqual(m0, msg);

            badMsgs.forEach(function(m) {
                assert.isNull(socket._parseMessage(m));
            });

            io._sockets = [];
            done();
        })(eventEmitter);
    });

    it('should have ws instances that should emit events and send them to client', function(done) {
        var eventEmitter    = new EventEmitter(),
            msgName         = 'foo',
            msgBody         = 'bar',
            count           = ATTEMPTS;

        eventEmitter.send = function(stringMsg, callback) {
            count--;

            if (!count) {
                assert.deepEqual(JSON.parse(stringMsg), {name: msgName, data: msgBody});

                callback(null);

                io._sockets = [];
                done();
            } else {
                callback(new Error('test error ' + (ATTEMPTS - count) + '. ignore it.'));
            }
        };

        io._handleConnection(function(socket) {
            socket.emit(msgName, msgBody);
        })(eventEmitter);
    });

    it('should have ws instances tha should have listener for message event', function(done) {
        var eventEmitter    = new EventEmitter();

        io._handleConnection(function(socket) {
            var listeners = socket._ws.listeners('message');

            assert.isArray(listeners);
            assert.lengthOf(listeners, 1);
            assert.equal(listeners[0], socket._handleMessage);

            io._sockets = [];
            done();
        })(eventEmitter);
    });
});
