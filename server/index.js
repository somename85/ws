'use strict';

var WebSocketServer = require('ws').Server,
    EventEmitter    = require('events').EventEmitter,
    jsonWebToken    = require('jsonwebtoken'),
    cookie          = require('cookie'),
    util            = require('util');


/**
 * @options
 * @type {object}
 * options = {
 *      secret: {string|buffer} - it is a string or buffer containing either the secret for HMAC algorithms,
 *      cookie: {string} - name cookie with token,
 *      port: {number},
 *      host: {string},
 *      attempts: {number} - amount sending attempts, default - 10
 * }
 */

/**
 *
 * @param options
 * @constructor
 */
function WSS(options) {
    var self = this;

    options = options || {};
    _checkOptions(options);

    self._attempts  = options.attempts || 10;
    self._host      = options.host;
    self._port      = +options.port;


    /**
     * all clients sockets
     * @type {Array}
     * @private
     */
    self._sockets = [];

    /**
     * it accept or decline socket connection
     * @param {object} info
     * @return {boolean}
     */
    self._verifyClient = function(info) {
        var token = cookie.parse(info.req.headers.cookie)[options.cookie];

        try {
            return !!jsonWebToken.verify(token, options.secret);
        } catch (err) {
            return !!self._handleError(err);
        }
    };

    /**
     * it return func that
     * improve client web Socket
     * and run callback
     * @param {function} callback
     * @private
     */
    self._handleConnection = function(callback) {
        return function(socket) {
            socket = new WS(socket, self._attempts);
            self._sockets.push(socket);
            callback(socket);
        }
    };

    /**
     * it send data to all clients
     * @param {string} eventName
     * @param {object} data - json object
     * @api public
     */
    self.emit = function(eventName, data) {
        var i = self._sockets.length;
        while (i--) self._sockets[i].emit(eventName, data);
    };

    /**
     * @param {error} err
     * @returns {null}
     * @private
     */
    self._handleError = function(err) {
        if (err) console.error(err);
        return null;
    };

    self.on = function(eventName, callback) {
        if (eventName !== 'connection') return;
        self._wss.on(eventName, self._handleConnection(callback));
    };

    self._wss = new WebSocketServer({
        port:           self._port,
        host:           self._host,
        verifyClient:   self._verifyClient
    });

    /**
     * @param options
     */
    function _checkOptions(options) {
        if (
            typeof options.secret !== 'string'
            && !Buffer.isBuffer(options.secret)
        ) throw new Error('secret must be string or buffer');

        if (typeof options.host !== 'string')   throw new Error('host must be string');
        if (typeof options.cookie !== 'string') throw new Error('cookie must be string');
        if (isNaN(parseFloat(options.port)) || !isFinite(options.port)) {
            throw new Error('port must be transformed to number');
        }
    }
}


/**
 * it create socket obj for event managing on server side
 * @constructor
 * @param {object} socket - required, socket
 * @param {number} attempts - required, emit attempts count
 */
function WS(socket, attempts) {
    WS.super_.apply(this, arguments);

    var self = this;

    /**
     * client webSocket instance
     * @type {null|socket}
     * @private
     */
    self._ws = socket;

    /**
     * emit attempts count
     * @type {number}
     */
    self._attempts = attempts;

    /**
     * save emit method from EventEmitter
     * @type {Function|WS.emit}
     * @private
     */
    self._emit = self.emit;

    /**
     * it parse message and emit it
     * @param {string} msg - string json object with name and data prop
     * @private
     */
    self._handleMessage = function(msg) {
        msg = self._parseMessage(msg);
        if (msg) self._emit(msg.name, msg.data);
    };

    /**
     * it parse simple json message
     * @param {string} msg
     * @returns {object|null}
     * @private
     */
    self._parseMessage = function(msg) {
        var result;

        try {
            result = JSON.parse(msg);
        } catch (err) {
            return self._handleError(err);
        }

        if (
            !util.isObject(result)
            || !result.name
            || !result.data
        ) return self._handleError(new Error('wrong msg: ' + msg));

        return result;
    };

    /**
     * it send data to current client
     * @param {string} eventName
     * @param {object} data - json object
     * @param {number|undefined} attempts - amount of sending attempts
     * @api public
     */
    self.emit = function(eventName, data, attempts) {
        attempts = attempts || 0;

        self._ws.send(JSON.stringify({name: eventName, data: data}), function(err) {
            self._handleError(err);
            if (err && (attempts < self._attempts)) setTimeout(function() { self.emit(eventName, data, attempts++); }, 100);
        });
    };

    /**
     * @param {error} err
     * @returns {null}
     * @private
     */
    self._handleError = function(err) {
        if (err) console.error(err);
        return null;
    };

    self._ws.on('message', self._handleMessage);
}

util.inherits(WS, EventEmitter);

module.exports = WSS;