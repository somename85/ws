# Websockets implementation

It is built as socket.io light replacement, so it should:

1. be as fast as possible
2. support only basic functions: send messages and subscribe
3. have api close to socket.io

#### Usage
First, set the configs at config.json, defaults:
    
    {
        "host": "127.0.0.1",
        "port": "3001",
        "url": "ws://127.0.0.1:3001",
        "secret": "secret",
        "cookie": "foo"
    }
    
Server side:

    WSS = require('path-to-ws/server'),
    io = new WS({
        secret:     {string|buffer} - required, it is a string or buffer containing either the secret for HMAC algorithms,
        cookie:     {string}        - required, name cookie with token,
        port:       {number}        - required, your ws port,
        host:       {string}        - required, your ws host
        attempts:   {number}        - optional, amount sending attempts, default - 10
    });
    
    
    io.on('connection', function(socket) {
        socket.on('hello', function(data) {
            console.log('incoming message', data);
            socket.emit('hi', {foo: 'bar'});
        });
    });
    
Client side:

    var WS      = require('path-to-ws/client'),
        socket  = new WS({
            url: {string} - required, your ws url
        });
        
    socket.emit('hello', {foo: 'bar'});
    socket.on('hi', function(data) {
        console.log('incoming message', data);
    });
        
You should build it with browserify.
#### Run example for more info:
    npm run example
or

    node your-path-to-ws/example.js
    
Navigate your browser to http://127.0.0.1:1337
#### Run tests:
    npm run test
or

    mocha
#### Build client script:
    npm run buildClient
    
#### Build example script:
    npm run buildExample
    