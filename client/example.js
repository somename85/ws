var WS              = require('./lib/ws'),
    config          = require('../config'),
    socket          = new WS({url: config.url}),

    outgoingMsg     = {foo: 'bar'},
    outgoingName    = 'hello',
    incomingName    = 'hi',

    bench           = {};


sayHello();
socket.on(incomingName, handleMsg);
document.getElementById('emit').addEventListener('click', sayHello);


function sayHello() {
    bench.start = new Date();
    socket.emit(outgoingName, outgoingMsg);
    console.log('\r\noutgoing event => name: ' + outgoingName + '; body:', outgoingMsg);
}
function handleMsg(data) {
    bench.result = new Date() - bench.start;
    console.log('incoming event => name: ' + incomingName + '; body:', data);
    console.log('response time:', bench.result, 'milliseconds.');
}