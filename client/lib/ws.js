'use strict';

var ReconnectingWebSocket   = require('./reconnectingWebsockets');

/**
 * @options
 * @type {object}
 * options = {
 *      url: 'ws://example.com' - url for web socket connection
 * }
 */

/**
 * it create socket obj for event managing on client side
 * @param options
 * @constructor
 */
function WS(options) {
    'use strict';

    var self = this;

    options = options || {};

    /**
     * it is storage for events, it have structure like { event1: [ listener1, listener2, ... ], event2: [ listener3, listener4, ... ], ... }
     * @type {object}
     * @private
     */
    self._events = {};

    /**
     * isOpen state boolean
     * @type {boolean}
     * @private
     */
    self._isOpen = false;

    /**
     * it is webSocket instance
     * @type {ReconnectingWebSocket}
     * @private
     */
    self._ws = new ReconnectingWebSocket(options.url);

    /**
     * it subscribe events
     * @param {string} event - name of event
     * @param {function} fn - listener of event
     * @api public
     */
    self.on = function(event, fn) {
        self._events[event] = self._events[event] || [];
        self._events[event].push(fn);
    };

    /**
     * it unsubscribe events
     * @param {string} event - name of event
     * @param {function} fn - listener of event
     * @api public
     */
    self.off = function(event, fn){
        if (event in self._events === false) return;
        self._events[event].splice(self._events[event].indexOf(fn), 1);
    };

    /**
     * it triggers events
     * @param {string} event - name of event
     * @private
     */
    self._emit = function(event /* , args... */){
        if (event in self._events === false) return;

        var i = self._events[event].length;
        while (i--) self._events[event][i].apply(this, Array.prototype.slice.call(arguments, 1));
    };

    /**
     * @cb
     * @param {error|null} error
     */

    /**
     * it sends event to server
     * @param {string} eventName
     * @param {object} data
     * @param {function} cb
     * @api public
     */
    self.emit = function(eventName, data, cb) {
        switch (self._isOpen) {
            case true:
                if (!cb) cb = self._handleError;
                self._ws.send(self._stringifyMessage({name: eventName, data: data}), cb);
                break;
            case false:
                setTimeout(function() { self.emit(eventName, data, cb); }, 100);
                break;
        }
    };

    /**
     * it parses message then emit proper event
     * @param {string} msg
     * @returns {*|null}
     * @private
     */
    self._handleMessage = function(msg) {
        msg = self._parseMessage(msg);

        if (msg) {
            //console.debug('incoming message', msg);
            if (!msg.name) return self._handleError(new Error('event must have name'));
            self._emit(msg.name, msg.data);
        }
    };

    /**
     * @private
     */
    self._handleConnection = function() {
        self._isOpen = true;
    };

    /**
     * @param {error} err
     * @returns {null}
     * @private
     */
    self._handleError = function(err) {
        console.error(err);
        return null;
    };

    /**
     * it parse simple json message
     * @param {string} msg
     * @returns {object|null}
     */
    self._parseMessage = function(msg) {
        try {
            return JSON.parse(msg.data);
        } catch (err) {
            return self._handleError(err);
        }
    };

    /**
     * stringify like simple json
     * @param {object} msg
     * @returns {string}
     */
    self._stringifyMessage = function(msg) {
        return JSON.stringify(msg);
    };

    self._ws.onopen     = self._handleConnection;
    self._ws.onmessage  = self._handleMessage;
}

if (module && module.exports) module.exports = WS;
if (window.navigator) window.WS = WS;